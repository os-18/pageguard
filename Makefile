# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

PROJECT=pageguard
BIN=build/${PROJECT}
DESTDIR=
PREFIX=usr/local

SRC=$(shell find source -name "*.d")

DC=ldc2
PHOBOS_LINKING=dynamic
AMALTHEA_LINKING=dynamic

PHOBOS_OPTS=$(shell ./env.sh $(DC) get_phobos_options $(PHOBOS_LINKING))
AMALTHEA_OPTS=$(shell ./env.sh $(DC) get_amalthea_options $(AMALTHEA_LINKING))
LINKING_OPTS=$(PHOBOS_OPTS) $(AMALTHEA_OPTS)
RELEASE_OPTS=$(shell ./env.sh $(DC) get_release_options)
DEBUG_OPTS=$(shell ./env.sh $(DC) get_debug_options)

DOC_DIR=$(shell ./env.sh $(DC) get_doc_dir)/$(PROJECT)

OUT=-of
ifeq ($(DC),gdc)
	OUT=-o 
endif

.PHONY: release debug install install-manpages install-help uninstall clean

release: $(SRC)
	mkdir -p build/
	$(DC) $^ $(OUT)$(BIN) $(RELEASE_OPTS) -Jsource $(LINKING_OPTS)

debug: $(SRC)
	mkdir -p build/
	$(DC) $^ $(OUT)$(BIN) $(DEBUG_OPTS) -Jsource $(LINKING_OPTS)

INST_BINDIR=$(DESTDIR)/$(PREFIX)/bin/
INST_BASHCOMPDIR=$(DESTDIR)/$(PREFIX)/share/bash-completion/completions/
INST_DOCDIR=$(DESTDIR)/$(PREFIX)/$(DOC_DIR)/
INST_HELPDIR_EN=$(DESTDIR)/$(PREFIX)/share/help/en_US/$(PROJECT)/
INST_HELPDIR_RU=$(DESTDIR)/$(PREFIX)/share/help/ru_RU/$(PROJECT)/
INST_HELPDIR_EO=$(DESTDIR)/$(PREFIX)/share/help/eo/$(PROJECT)/
INST_MANDIR_EN=$(DESTDIR)/$(PREFIX)/share/man/man1
INST_MANDIR_RU=$(DESTDIR)/$(PREFIX)/share/man/ru/man1
INST_MANDIR_EO=$(DESTDIR)/$(PREFIX)/share/man/eo/man1

# now pageguard doesn't have man-pages
install-man-pages:
	mkdir -p "$(INST_MANDIR_EN)" "$(INST_MANDIR_RU)" "$(INST_MANDIR_EO)"
	gzip -9 -k -n help/$(PROJECT).1
	gzip -9 -k -n help/$(PROJECT).ru.1
	gzip -9 -k -n help/$(PROJECT).eo.1
	install help/$(PROJECT).1.gz    "$(INST_MANDIR_EN)/$(PROJECT).1.gz"
	install help/$(PROJECT).ru.1.gz "$(INST_MANDIR_RU)/$(PROJECT).1.gz"
	install help/$(PROJECT).eo.1.gz "$(INST_MANDIR_EO)/$(PROJECT).1.gz"
	rm help/*.1.gz

install-help:
	mkdir -p "$(INST_HELPDIR_EN)" "$(INST_HELPDIR_RU)" "$(INST_HELPDIR_EO)"
	cp help/help_en_US.txt "$(INST_HELPDIR_EN)/help.txt"
	cp help/help_ru_RU.txt "$(INST_HELPDIR_RU)/help.txt"
	cp help/help_eo.txt    "$(INST_HELPDIR_EO)/help.txt"

install: install-help
	mkdir -p "$(INST_BINDIR)" "$(INST_BASHCOMPDIR)" "$(INST_DOCDIR)"
	install "$(BIN)" "${INST_BINDIR}"
	cp source/_$(PROJECT) "$(INST_BASHCOMPDIR)/$(PROJECT)" || true
	cp copyright $(INST_DOCDIR)

uninstall:
	rm -f $(INST_BINDIR)/$(PROJECT)
	rm -f $(INST_BASHCOMPDIR)/$(PROJECT)
	rm -rf $(INST_DOCDIR)
	rm -rf $(INST_HELPDIR_EN) $(INST_HELPDIR_RU) $(INST_HELPDIR_EO)
	rm -f $(INST_MANDIR_EO)/$(PROJECT).1.gz
	rm -f $(INST_MANDIR_EN)/$(PROJECT).1.gz
	rm -f $(INST_MANDIR_RU)/$(PROJECT).1.gz

clean:
	rm -rf build/
